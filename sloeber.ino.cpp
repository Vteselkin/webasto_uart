#ifdef __IN_ECLIPSE__
//This is a automatic generated file
//Please do not modify this file
//If you touch this file your change will be overwritten during the next build
//This file has been generated on 2021-03-02 12:57:28

#include "Arduino.h"
#include "LiquidCrystal_I2C.h"
#include <avr/wdt.h>
#include <DallasTemperature.h>

void setup() ;
void loop() ;
void ParamPrint() ;
void sendMessage(const byte *command, const size_t size) ;
void corTemp(const byte &byte_T) ;
void Heater_BUS() ;
void Tone(const word frequency, const word duration) ;
void LCD(char message[]) ;
void LCD_MESS(byte message[]) ;
bool scanI2CBus() ;

#include "WebastoUART.ino"


#endif
