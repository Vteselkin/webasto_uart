#include "LiquidCrystal_I2C.h"
#include <avr/wdt.h>
#include <DallasTemperature.h>
LiquidCrystal_I2C lcd(0x3F, 20, 4); // @suppress("Abstract class cannot be instantiated")
//#include <SoftwareSerial.h>  // ���� ������, �� ������� ����� �-���� �������

//       #define RX 7          // ��� ���� �-���� ��������, �������� �� ���� ������
//       #define TX 8          // ��� ���� �-���� ��������, �������� �� ���� ������
//       SoftwareSerial K_LINE   (RX, TX);

#define K_LINE Serial      //UART ��� ���������� � ����� �����
#define TX 1
#define BUFSIZE 140
// ����� ���� Arduino � ������������ ��������
#define PIN_DS18B20 13

// ������� ������ OneWire
OneWire oneWire(PIN_DS18B20);

// ������� ������ DallasTemperature ��� ������ � ���������, ��������� ��� ������ �� ������ ��� ������ � 1-Wire.
DallasTemperature dallasSensors(&oneWire);

// ����������� ������ ��� �������� ������ ����������
DeviceAddress sensorAddress;
uint32_t curmillis = 0;    // ������ ���������� �������
//Pin for speaker
const byte tonePin = 4;

// ������� ��� ������ ���/TTE

const byte START_SESSION[] { 0x81 };
const byte REQUEST_2A10101[] { 0x2A, 0x01, 0x01 };
const byte REQUEST_2A10102[] { 0x2A, 0x01, 0x02 };
const byte REQUEST_2A10105[] { 0x2A, 0x01, 0x05 };
const byte REQUEST_DTC[] { 0xA1 };
//const byte START_TTC[] { 0x31, 0x22, 0x01 };
byte START_TTC[] { 0x31, 0x22, 0xFF };

const byte STOP_TTC[] { 0x31, 0x22, 0x00 };

byte w_bus_init = 1;        //������� ��������� ������������� ���� (300ms LOW, 50ms HIGH, 25ms LOW, 3025ms HIGH ��� TTC)
uint32_t prevInitreset = 0;                     //������ ������ ����� ����
bool Initreset = 0;                             //������ ������ ����� ����
byte initcount = 0;
bool initComplete = false;

//--------------------------

int temperature;
float napruga;
int nagnetatel;
bool Nagnetatel;
int plug;
bool Plug;
float fuelpump = 1.0;
bool Fuelpump;
bool Blower;
bool Flamestatus;

bool Waterpump;
bool StartCommand;
int DTCstatus;

void setup() {
  wdt_disable();
  pinMode(tonePin, OUTPUT);
  scanI2CBus();
  lcd.init();
  lcd.backlight();
  lcd.clear();
  lcd.setCursor(4, 1);
  lcd.print("WEBASTO v4.4 ");
  Tone(400, 700);
  delay(10000);
  K_LINE.begin(10400);
  wdt_enable (WDTO_8S);
}

void loop() {
  curmillis = millis();
  Heater_BUS();
  ParamPrint();

  wdt_reset();
}
void ParamPrint() {
  static uint32_t prev = 0;

  if (curmillis - prev > 2000) {
    dallasSensors.requestTemperatures();
    float tempC = dallasSensors.getTempCByIndex(0);
     prev = curmillis;
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("T=");
    lcd.print(temperature);
    lcd.print("C ");
    lcd.print("V=");
    lcd.print(napruga);
    lcd.setCursor(13, 0);
    lcd.print("T2=");
    lcd.print(tempC);
    lcd.setCursor(0, 1);
    lcd.print("BEH=");
    lcd.print(nagnetatel);
    lcd.print("% ");
    lcd.print("  CBE4A=");
    lcd.print(plug);
    lcd.print("% ");
    lcd.setCursor(0, 2);
    lcd.print("DIZEL=");
    lcd.print(fuelpump);
    lcd.print("Hz ");

  }
}

void sendMessage(const byte *command, const size_t size) {

  Initreset = 1;
  prevInitreset = curmillis;  // ��������� ������� ������ �����
  const byte siZe = size + 4;
  byte Mes[siZe];
  byte Checksum = 0;
  for (byte i = 0; i < siZe; i++) {
    if (i == 0) {
      Mes[i] = size;
      bitWrite(Mes[i], 7, 1);
    }
    if (i == 1)
      Mes[i] = 0x51;
    if (i == 2)
      Mes[i] = 0xF1;
    if (i == 3) {
      for (byte t = 0; t < size; t++) {
        Mes[i] = command[t];
        Checksum += Mes[i];
        K_LINE.write(Mes[i]);
        K_LINE.read();
        i++;
      }
    }
    if (i != siZe - 1)
      Checksum += Mes[i];
    else
      Mes[i] = Checksum;
    K_LINE.write(Mes[i]);
    K_LINE.read();
  }

}

void corTemp(const byte &byte_T) {

  if (byte_T >= 242)
    temperature = -40;
  else if (byte_T >= 229 && byte_T <= 241)
    temperature = map(byte_T, 241, 229, -35, -12);
  else if (byte_T == 228)
    temperature = -11;
  else if (byte_T == 227)
    temperature = -10;
  else if (byte_T >= 217 && byte_T <= 226)
    temperature = map(byte_T, 226, 217, -8, 1);
  else if (byte_T >= 211 && byte_T <= 216)
    temperature = map(byte_T, 216, 211, 1, 5);
  else if (byte_T >= 168 && byte_T <= 210)
    temperature = map(byte_T, 210, 168, 6, 30);
  else if (byte_T >= 124 && byte_T <= 167)
    temperature = map(byte_T, 167, 124, 30, 50);
  else if (byte_T >= 83 && byte_T <= 123)
    temperature = map(byte_T, 123, 83, 50, 70);
  else if (byte_T >= 67 && byte_T <= 82)
    temperature = map(byte_T, 82, 67, 71, 80);
  else if (byte_T >= 53 && byte_T <= 66)
    temperature = map(byte_T, 66, 53, 81, 90);
  else if (byte_T >= 42 && byte_T <= 52)
    temperature = map(byte_T, 52, 42, 91, 100);
  else if (byte_T >= 34 && byte_T <= 41)
    temperature = map(byte_T, 41, 34, 101, 110);
  else if (byte_T >= 27 && byte_T <= 33)
    temperature = map(byte_T, 33, 27, 111, 120);
  else if (byte_T >= 20 && byte_T <= 26)
    temperature = map(byte_T, 26, 20, 122, 133);
  else if (byte_T <= 19)
    temperature = 135;

  if (temperature >= 78) {
    Tone(1000, 100);
  }
}

void Heater_BUS() {

  static byte header = 0;            // ��������� ���������
  static byte message_size = 0;      // ������ ���� ������������ ���������, ���-�� ����
  static byte j = 2;                 // ���������
  static byte n = 2;
  const byte bufsize = BUFSIZE;      // ������ ������ ��������� ���������
  static byte buf[bufsize] = { 0 };   // ����� ��������� ���������
  static byte checksum = 0;          // ����������� ����� ��������� ���������
  static uint32_t prevRESETheader = 0; // ������ ������ ��������� ���� � ������ ����� ��������� ������ ����������
  static bool RESETheader_timer = 0; // ������ ������ ��������� ���� � ������ ����� ��������� ������ ����������
  static uint32_t timerInit = 0;     // ������ ��������� ����� ���� W-BUS
  static uint32_t Prev_PeriodW_BusMessage = 0; //������ ������������� �������� ��������� ��������� ����� � ���� W-Bus

  if (w_bus_init == 1) {
    K_LINE.end();
    pinMode(TX, OUTPUT);
    digitalWrite(TX, 0);
    timerInit = curmillis;
    ;
    w_bus_init = 2;
    LCD("WEBASTO START INIT ");
    lcd.print(initcount);
    Tone(1000, 100);
  }
  if (w_bus_init == 2 && curmillis - timerInit > 299) {
    timerInit = curmillis;
    digitalWrite(TX, 1);
    w_bus_init = 3;
  }
  if (w_bus_init == 3 && curmillis - timerInit > 49) {
    timerInit = curmillis;
    digitalWrite(TX, 0);
    w_bus_init = 4;
  }
  if (w_bus_init == 4 && curmillis - timerInit > 24) {
    timerInit = curmillis;
    digitalWrite(TX, 1);
    w_bus_init = 5;
  }
  if (w_bus_init == 5 && curmillis - timerInit > 3024) {
    K_LINE.begin(10400);
    w_bus_init = 6;
  }
  if (w_bus_init == 6 && curmillis - timerInit > 1000) {
    timerInit = curmillis;
    sendMessage(START_SESSION, sizeof(START_SESSION));
    initcount++;

    if (initcount == 3 || initcount == 6 || initcount == 9) {
      w_bus_init = 1;

    }
    if (initcount >= 10) {
      w_bus_init = 0;
      initcount = 0;
    }
  }

  if (K_LINE.available()) {

    // ������ ����� ����
    if (header == 0) {
      buf[0] = K_LINE.read();
      if (!bitRead(buf[0], 6) && bitRead(buf[0], 7)) {
        header++;
        RESETheader_timer = 1;
        prevRESETheader = curmillis;
      }

    }

    // ������ ����� ����
    else if (header == 1) {
      buf[1] = K_LINE.read();
      if (buf[1] == 0xF1) {
        header++;
      } else {
        header = 0;
        RESETheader_timer = 0;
      }
    }

    // ������ ����� ����
    else if (header == 2) {

      buf[2] = K_LINE.read();
      if (buf[2] == 0x51 || buf[2] == 0x10) {
        message_size = buf[0];
        if (buf[0] != 0x80) {
          header = 4;
          message_size &= ~0x80;
          j = 3;
          n = 3;
        } else {
          header = 3;
          j = 4;
          n = 4;
        }
        if (message_size > bufsize)
          message_size = bufsize;
        checksum = 0;
      } else {
        header = 0;
        RESETheader_timer = 0;
      }

    }
// ���� ������ ��������� ������ � �������������� ����� (������� ���� 0x80) ������ ���� �������������� ����:
    else if (header == 3) {

      buf[3] = K_LINE.read();
      message_size = buf[3];
      if (message_size > bufsize)
        message_size = bufsize;
      checksum = 0;
      header = 4;
    }

    // ����� ���� ���������
    else if (header == 4 && j < message_size + n + 1) {
      buf[j] = K_LINE.read();
      if (j < message_size + n)
        checksum += buf[j]; // ������� ��

      if (j == message_size + n)
        header = 5;
      j++;
    }

  } // end of K_LINE.available()

  // ��������� �������, ���������
  if (header == 5) {

    for (byte i = 0; i < n; i++)
      checksum += buf[i]; // ���������� � ����������� ����� ����� �����

    // ���� ����������� ����� �����:
    if (buf[message_size + n] == checksum) {

      if (buf[n] == 0xC1) {
        w_bus_init = 10;
        initcount = 0;
        delay(50);
        if (!initComplete) {
          initComplete = true;
          Tone(100, 300);
          LCD("INIT COMPLETE ");
        }
        //Serial.println ("Init is good!"); // ���� ���� ������ �������, ��� ����� �� ����������� �������
      }
      if (buf[n] == 0x6A && buf[n + 1] == 0x01) {
        corTemp(buf[n + 2]);
        napruga = (float) buf[n + 4] / 14.64;
      }
      if (buf[n] == 0x6A && buf[n + 1] == 0x02) {
        nagnetatel = buf[n + 2] * 100 / 255;
        plug = buf[n + 3];
        if (buf[n + 5] != 0)
          fuelpump = 100.0 / (float) buf[n + 5];
        else
          fuelpump = 0;
        Nagnetatel = bitRead(buf[n + 6], 0);
        Plug = bitRead(buf[n + 6], 1);
        Waterpump = bitRead(buf[n + 6], 2);
        Fuelpump = bitRead(buf[n + 6], 3);
        Blower = bitRead(buf[n + 6], 4);
        StartCommand = bitRead(buf[n + 7], 2);
        Flamestatus = bitRead(buf[n + 7], 5);
      }
      if (buf[n] == 0xE1 && buf[n + 1] == 0xFF && buf[n + 2] == 0xFF) {
        DTCstatus = buf[n + 3];
      }

    } // end of good CheckSum

// ���� ����������� ����� �� �������:
    else {
      LCD("CRC FAIL!!!");
      Tone(100, 500);
    }
    message_size = 0;
    header = 0;
    RESETheader_timer = 0;
    j = 3;
    checksum = 0;
  }

// ������ ������ ��������� ���� ������ ���������� �� ����� ����� ���������
  if (RESETheader_timer && curmillis - prevRESETheader > 500) {
    RESETheader_timer = 0;
    header = 0;
    LCD("HEADER RESET");
  }

  if (Initreset && curmillis - prevInitreset > 17000) {
    Initreset = 0;
    w_bus_init = 0;
    LCD("RESET INIT");
    Tone(100, 500);
  }  // ����� �����, ���� ������ ����� 17 ��� ����� �������� ���������� ���������

  if (w_bus_init >= 10 && curmillis - Prev_PeriodW_BusMessage > 2000) {
    static byte mescount;
    if (mescount >= 0 && mescount <= 16) {
      if (w_bus_init == 10) {
        sendMessage(REQUEST_2A10101, sizeof(REQUEST_2A10101));
       // LCD_MESS(REQUEST_2A10101);
        w_bus_init = 11;
      } else if (w_bus_init == 11) {
        sendMessage(REQUEST_2A10102, sizeof(REQUEST_2A10102));
      //  LCD_MESS(REQUEST_2A10102);
        w_bus_init = 12;
      } else if (w_bus_init == 12) {
        sendMessage(START_TTC, sizeof(START_TTC));
      //  LCD_MESS(START_TTC);
        START_TTC[2] = 0x01;
        w_bus_init = 10;
      }
    } else if (mescount >= 17) {
      sendMessage(REQUEST_DTC, sizeof(REQUEST_DTC));
    //  LCD_MESS(REQUEST_DTC);
      mescount = 0;
    }
    mescount++;
    Prev_PeriodW_BusMessage = curmillis;
  }
}

/**
 * Method of signaling through the system speaker
 */
void Tone(const word frequency, const word duration) {
  if (frequency == 0 && duration == 0) {
    tone(tonePin, 500, 10);
  } else {
    tone(tonePin, frequency, duration);
  }
}

void LCD(char message[]) {
  lcd.clear();
  lcd.setCursor(0, 1);
  lcd.print(message);
}

void LCD_MESS(byte message[]) {
  lcd.clear();
  lcd.setCursor(0, 2);
  for (byte i = 0; i < sizeof(message); i++) {
    lcd.print(message[i]);
    lcd.print(" ");
  }
}

/**
 * The method of searching for the LCD address on the I2C bus
 */
bool scanI2CBus() {
  byte adress = 0;
  if (adress < 10 || adress > 127) {
    byte nDevices = 0;
    for (byte address = 10; address <= 127; address++) {
      Wire.beginTransmission(address);
      byte error = Wire.endTransmission();
      if (error == 0) {
        adress = address;
        nDevices++;
      }
    }
    if (nDevices == 0) {
      return false;
    } else if (nDevices > 1) {
      return false;
    }

  }
  lcd = LiquidCrystal_I2C(adress, 20, 4);
  lcd.init();
  lcd.backlight();
  return true;

}